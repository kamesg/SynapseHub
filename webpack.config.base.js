/**
 * Base webpack config used across other specific configs
 */

import path from 'path';
import webpack from 'webpack';
import { dependencies as externals } from './app/package.json';

export default {
  externals: Object.keys(externals || {}),

  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          cacheDirectory: true
        }
      }
    }]
  },

  output: {
    path: path.join(__dirname, 'app'),
    filename: 'bundle.js',
    // https://github.com/webpack/webpack/issues/1114
    libraryTarget: 'commonjs2'
  },

  /**
   * Determine the array of extensions that should be used to resolve modules.
   */
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
      path.join(__dirname, 'app'),
      'node_modules',
    ],
    alias: {
      components: path.resolve(__dirname, 'app/interface/components'),
      atoms: path.resolve(__dirname, 'app/interface/components/atomic/atoms'),
      molecules: path.resolve(__dirname, 'app/interface/components/atomic/molecules'),
      organisms: path.resolve(__dirname, 'app/interface/components/atomic/organisms'),
      pages: path.resolve(__dirname, 'app/interface/components/atomic/pages'),

      /* Higer-Order Components |  */
      containers: path.resolve(__dirname, 'app/interface/containers'),
      layout: path.resolve(__dirname, 'app/interface/layout'),
      forms: path.resolve(__dirname, 'app/interface/forms'),

      /* Store | State Management */
      store: path.resolve(__dirname, 'app/interface/store'),
      
      /* Theme */
      theme: path.resolve(__dirname, 'app/interface/theme'),

      /*--- Logic ---*/
      settings: path.resolve(__dirname, 'app/logic/settings'),
      services: path.resolve(__dirname, 'app/logic/services'),
      state: path.resolve(__dirname, 'app/logic/state'),

      /* Storage */
      storage: path.resolve(__dirname, 'app/logic/storage')
    }
  },

  plugins: [
    new webpack.NamedModulesPlugin(),
  ],
};
