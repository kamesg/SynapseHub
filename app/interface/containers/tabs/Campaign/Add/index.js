import React, { Component } from 'react';

import CampaignAddFormContainer from 'containers/forms/Campaign/Add'

export default class CampaignAddTab extends Component {
  render() {
    return (
      <div>
        <CampaignAddFormContainer />
      </div>
    );
  }
}
