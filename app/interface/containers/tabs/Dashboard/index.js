// @flow
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class DashboardTab extends Component {
  static propTypes = {
    loading: PropTypes.bool,
  }

  static defaultProps = {
    loading: true
  }
  render() {
    return (
      <div>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: true, //fromStatus.isLoading(state, FORM_SETTINGS),
  passing: ""  //fromStatus.isPassing(state, FORM_SETTING_PASSING),
})

const mapDispatchToProps = (dispatch, { }) => ({
  handleSubmit: () => dispatch.formSave
})

export default connect(mapStateToProps, mapDispatchToProps)(DashboardTab)
