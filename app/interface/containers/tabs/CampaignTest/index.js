import React, { Component } from 'react';

import CampaignTestFormContainer from 'containers/forms/Campaign/Test'


export default class CampaignTestTab extends Component {
  render() {
    return (
      <div>
        <h3>Campaign Test</h3>
        <CampaignTestFormContainer />
      </div>
    );
  }
}
