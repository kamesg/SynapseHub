/* ------------------------- External Dependencies -------------------------- */
import React, { PropTypes } from 'react'
import { Field } from 'redux-form'
import styled from 'styled-components'

/* ------------------------- Internal Dependencies -------------------------- */
import Button from 'atoms/Button'
import FieldComponent from 'molecules/Field'
import ReduxField from 'organisms/ReduxField'

const CommunityPersonAddFormWrapper = styled.form`
  display: block;
`

/* ------------------------------- Component -------------------------------- */
const CommunityPersonAddForm = ({handleSubmit, isSubmitting }) => {
  return (
    <CommunityPersonAddFormWrapper method="POST" onSubmit={handleSubmit}>
      <Field name="_csrf" type="hidden" component="input"/>
      <Field name="personNameFirst" label="First Name" component={ReduxField} type="text" />
      <Field name="personNameLast" label="Last Name" component={ReduxField} type="text" />
      <Field name="personMiddleLast" label="Middle Name" component={ReduxField} type="text" />
      <div>
        <Button type="submit" disabled={isSubmitting}>Import Current Projects</Button>
      </div>
    </CommunityPersonAddFormWrapper>
  )
}

/* ------------------------- Component Properties --------------------------- */
CommunityPersonAddForm.propTypes = {
  handleSubmit: PropTypes.func,
  isSubmitting: PropTypes.bool
}

/* ---------------------------- Export Package ------------------------------ */
export default CommunityPersonAddForm