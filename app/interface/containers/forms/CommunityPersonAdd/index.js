/* ------------------------- External Dependencies -------------------------- */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'

/* ------------------------- Internal Dependencies -------------------------- */
import SimpleStorage from 'storage/SimpleStorage';
import { createValidator, required } from 'services/validation'
import { fromForm } from 'store/logic/selectors'

/* ------------------------ Initialize Dependencies ------------------------- */
let projects = new SimpleStorage('projects');

/* --------------------------- Child Components ----------------------------- */
import CommunityPersonAddForm from './components/CommunityPersonAddForm'

/* ------------------------------- Component -------------------------------- */
const CommunityPersonAddContainer = props => <CommunityPersonAddForm { ...props} />

/* ------------------------- Component Properties --------------------------- */
const onSubmit = (data, dispatch) => new Promise((resolve, reject) => {

})

const validate = createValidator({
  personNameFirst: [required],
})

const mapStateToProps = (state) => ({
  initialValues: {
    _csrf: fromForm.getCsrfToken(state)
  }
})

export const config = {
  form: 'CommunityPersonAdd',
  fields: [
    'personNameFirst',
  ],
  destroyOnUnmount: true,
  onSubmit,
  validate
}


/* ---------------------------- Export Package ------------------------------ */
export default connect(mapStateToProps)(reduxForm(config)(CommunityPersonAddContainer))
/* ----------------------------- Documentation ------------------------------ */