import React, { PropTypes } from 'react'
import styled from 'styled-components'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import { palette } from 'styled-theme'

import {List, ListItem} from 'material-ui/List';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import ActionDashboard from 'material-ui/svg-icons/action/dashboard';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ActionExtension from 'material-ui/svg-icons/action/extension';
import ActionFindReplace from 'material-ui/svg-icons/action/find-replace';
import ActionSettingsInputComposite from 'material-ui/svg-icons/action/settings-input-composite';
import ActionSupervisor from 'material-ui/svg-icons/action/supervisor-account';
import CommunicationForum from 'material-ui/svg-icons/communication/forum';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import DeviceStorage from 'material-ui/svg-icons/device/storage';
import DeviceWidgets from 'material-ui/svg-icons/device/widgets';
import ImageViewCompact from 'material-ui/svg-icons/image/view-compact';
import NavigationApps from 'material-ui/svg-icons/navigation/apps';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import NavigationMoreVert from 'material-ui/svg-icons/navigation/more-vert';
// import NavigationMoreHorz from 'material-ui/svg-icons/navigation/more-horz';
import NotifcationEventAvailable from 'material-ui/svg-icons/notification/event-available';
import NotifcationEventBusy from 'material-ui/svg-icons/notification/event-busy';
import ActionInfo from 'material-ui/svg-icons/action/info';
import SocialHot from 'material-ui/svg-icons/social/whatshot';
import SocialPublic from 'material-ui/svg-icons/social/public';
// import SocialNotification from 'material-ui/svg-icons/social/notification';

import SocialGroup from 'material-ui/svg-icons/social/group';
import SocialGroupAdd from 'material-ui/svg-icons/social/group-add';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';

const Nav = styled.nav`
  color: #3e3e3e;
  display: inline-block;
  list-style: none;
  width: 100%;

  a {
    font-weight: 300;
    color: ${palette('#3e3e3e', 0)};
    font-size: 1.15rem;
    font-weight: normal;
    &.active {
      color: ${palette('#3e3e3e', 0)};
    }
  }

  svg {
    margin: 0 !important;
  }
`

const ListItemStyle = {
  color: "#3e3e3e",
  fontFamily: "Raleway",
  padding: "5px 10px 5px 10px"
}

const SidebarNavigation = (props) => {
  return (

    <Nav {...props}>
      <Subheader style={{color: "#3e3e3e"}} >Dashboard</Subheader>
      <Divider />
      <List>
        <Link to={"/overview"}>
          <ListItem
            style={ListItemStyle}
            innerDivStyle={ListItemStyle}
            primaryText="Overview"
            rightIcon={<ActionDashboard color={"rgba(35, 116, 224, 0.84)"}/>}
          />
          </Link>
          <Link to="/test">
          <ListItem
            style={ListItemStyle}
            innerDivStyle={ListItemStyle}
            primaryText="Test"
            rightIcon={<ActionDashboard color={"rgba(35, 116, 224, 0.84)"}/>}
          />
          </Link>
      </List>
      <Subheader style={{color: "#3e3e3e"}} >System</Subheader>
      <Divider />
      <List>
        <Link to="/settings">
          <ListItem
            style={ListItemStyle}
            innerDivStyle={ListItemStyle}
            primaryText="About"
            rightIcon={<CommunicationForum color={"rgba(35, 116, 224, 0.84)"} />}
          />
        </Link>
        <Link to="/documentation">
          <ListItem
            style={ListItemStyle}
            innerDivStyle={ListItemStyle}
            primaryText="Docs"
            rightIcon={<ActionAssignment  color={"rgba(35, 116, 224, 0.84)"} />}
          />
        </Link>
      </List>
    </Nav>

  )
}

SidebarNavigation.propTypes = {
  reverse: PropTypes.bool,
}

export default SidebarNavigation
