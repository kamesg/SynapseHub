/* ------------------------- External Dependencies -------------------------- */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import GoogleMapReact from 'google-map-react';


/* ------------------------- Internal Dependencies -------------------------- */

/* ------------------------ Initialize Dependencies ------------------------- */

/* --------------------------- Child Components ----------------------------- */

/* ---------------------------- Module Package ------------------------------ */
class Container extends Component {

  /*--- Property Types ---*/
  static propTypes = {
    loading: PropTypes.bool
  }

  /*--- Default Properties ---*/
  static defaultProps = {
    loading: true
  }

  /*--- Component Mount ---*/
  componentDidMount() {
    
  }

  /*--- Render ---*/
  render() {
    return (
    <GoogleMapReact 
      bootstrapURLKeys={{key: 'AIzaSyBM6-HVe9citP3Sxo7n2IEeOQocvBJ9l80',language: 'en',}}
      center={{lat: 37.9642301, lng: -122.501506}} defaultZoom={15} >

      
    </GoogleMapReact>
    )
  }
}
/* ---------------------------- Module Methods ------------------------------ */
/* Map State To Props */
const mapStateToProps = state => ({
  loading: true,
})

/* Map Dispatch To Props */
const mapDispatchToProps = (state) => ({

})
/* ---------------------------- Export Package ------------------------------ */
export default connect(mapStateToProps, mapDispatchToProps)(Container)
/* ----------------------------- Documentation ------------------------------ */
