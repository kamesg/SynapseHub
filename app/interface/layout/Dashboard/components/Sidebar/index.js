/* ------------------------- External Dependencies -------------------------- */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'


/* ------------------------- Internal Dependencies -------------------------- */
import SidebarNavigation from 'containers/routing/SidebarNAvigation'

/* ------------------------ Initialize Dependencies ------------------------- */

/* --------------------------- Child Components ----------------------------- */
import Sidebar from './styled-components/SidebarStyled'

/* ---------------------------- Module Package ------------------------------ */
class SidebarContainer extends Component {
  static propTypes = {
    mode: PropTypes.func,
    refresh: PropTypes.func,
    loading: PropTypes.bool,
    children: PropTypes.object
  }
  static defaultProps = {
    loading: true
  }

  /* Component Mount */
  componentDidMount() {
    
  }

  /* Render */
  render() {
    return (
    <Sidebar>
      <SidebarNavigation></SidebarNavigation>/>
    </Sidebar>
    )
  }
}
/* ------------------------- Begin Module Methods --------------------------- */

/* Map State To Props */
const mapStateToProps = state => ({
  list: "[{name:'hello'}]",
  loading: true,
})

/* Map Dispatch To Props */
const mapDispatchToProps = (state) => ({
  mode: () => dispatch(),
  refresh: () => dispatch()
})
/* ---------------------------- Export Package ------------------------------ */
export default connect(mapStateToProps, mapDispatchToProps)(SidebarContainer)

/* ----------------------------- Documentation ------------------------------ */
