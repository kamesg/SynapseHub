/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const SidebarStyledWrapper = styled.aside`
  background-color: #FFF;
  ${''/* box-shadow: 0 2px 21px 0 rgba(0, 0, 0, 0.1), 4px 3px 20px 0px rgba(0, 0, 0, 0.58); */}
  bottom: 0; left: 0;
  top: ${key(['layout', 'sidebar','top'])};
  position: fixed;
  width: ${key(['layout', 'sidebar','width'])};
  z-index: ${key(['layout', 'sidebar','zindex'])};
`

/* ------------------------- Component Properties --------------------------- */
SidebarStyledWrapper.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const SidebarStyled = ({ ...props }) => {
  return (
    <SidebarStyledWrapper {...props} />
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default SidebarStyledWrapper