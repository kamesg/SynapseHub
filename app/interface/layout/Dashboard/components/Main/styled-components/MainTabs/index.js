/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const MainTabsWrapper = styled.div`
  background-color: ${key(['layout', 'main', 'tabs', 'background'])};
  box-shadow: 0px 2px 6px rgba(0,0,0,0.19), 0px 3px 3px rgba(0, 0, 0, 0.16);
  left: 0;
  height: ${key(['layout', 'main', 'tabs', 'height'])};
  overflow: hidden;
  padding: ${key(['layout', 'main', 'tabs', 'padding'])};
  position: absolute;
  top: 0;
  width: ${key(['layout', 'main', 'tabs', 'width'])};
  z-index: 5;

  & *:first-of-type {
    margin-top: 0;
  }
`

/* ------------------------- Component Properties --------------------------- */
MainTabsWrapper.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const MainTabs = ({ ...props }) => {
  return (
    <MainTabsWrapper {...props}>
      {children}
    </MainTabsWrapper>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default MainTabsWrapper