/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const MainStyledWrapper = styled.main`
  background-color: ${key(['layout', 'main','background'])};
  box-shadow: ;
  bottom: 0;
  top: ${key(['layout', 'offsetHeight'])};
  left: ${key(['layout', 'offsetWidth'])};
  padding: ${key(['layout', 'main', 'padding'])};
  position: fixed;
  width: ${key(['layout', 'display'])};
  z-index: 5;
`

/* ------------------------- Component Properties --------------------------- */
MainStyledWrapper.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const MainStyled = ({ ...props }) => {
  return (
    <MainStyledWrapper {...props}>
      {children}
    </MainStyledWrapper>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default MainStyledWrapper