/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'


/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const MainTrayWrapper = styled.div`
  background-color: ${key(['layout', 'main', 'tray', 'background'])};
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
  left: 0;
  min-height: ${key(['layout', 'main', 'tray', 'height'])};
  padding: ${key(['layout', 'main', 'tray', 'padding'])};
  position: absolute;
  top: ${key(['layout', 'main', 'tray', 'top'])};
  width: ${key(['layout', 'main', 'tray', 'width'])};
  z-index: -5;

  & *:first-of-type {
    margin-top: 0;
  }
`

/* ------------------------- Component Properties --------------------------- */
MainTrayWrapper.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const MainTray = ({ ...props }) => {
  return (
    <MainTrayWrapper {...props}>
        {children}
    </MainTrayWrapper>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default MainTrayWrapper