/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const width = key(['layout', 'main', 'viewport', 'width'])
const MainViewportWrapper = styled.main`
  background-color: ${key(['layout', 'main', 'viewport', 'background'])};
  box-shadow: ;
  color: #3e3e3e;
  height: ${key(['layout', 'main', 'viewport', 'height'])};
  left: ${key(['layout', 'main', 'viewport', 'left'])};
  padding-bottom: 25px;
  padding-left: 25px;
  padding-right: 25px;
  padding-top: 25px;
  position: absolute;
  top: ${key(['layout', 'main', 'viewport', 'top'])};
  width: ${width};
  z-index: 5;
`

/* ------------------------- Component Properties --------------------------- */
MainViewportWrapper.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const MainViewport = ({ ...props }) => {
  return (
    <MainViewportWrapper {...props}>
      {children}
    </MainViewportWrapper>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default MainViewportWrapper