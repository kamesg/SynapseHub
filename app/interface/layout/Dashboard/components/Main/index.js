/* ------------------------- External Dependencies -------------------------- */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import styled from 'styled-components'

/* ------------------------- Internal Dependencies -------------------------- */
import SelfContainedRouting from 'containers/routing/SelfContainedRouting'
import GoogleMapFeed from 'containers/interactive/map/GoogleMapFeed'
import OverviewTray from 'pages/Overview/components/OverviewTray'

import Button from 'atoms/Button'

/* ------------------------ Initialize Dependencies ------------------------- */

/* --------------------------- Child Components ----------------------------- */
/* Wrapper */
import Main from './styled-components/MainStyled'

/* Sub-Components */
import MainTabs from './styled-components/MainTabs'
import MainTray from './styled-components/MainTray'
import MainViewport from './styled-components/MainViewport'

const Dashboard = () => (
  <div>
    <h2>Dashboard</h2>
  </div>
)

const Overview = () => (
  <div>
    <h2>Overview</h2>
  </div>
)

const Settings = () => (
  <div>
    <h2>Settings</h2>
  </div>
)

const Test = () => (
  <div>
    <h2>Test</h2>
  </div>
)

const ButtonGrouped = styled(Button)`
  margin-right:10px;
`


/* ---------------------------- Module Package ------------------------------ */
class MainContainer extends Component {
  static propTypes = {

  }
  static defaultProps = {

  }

  /* Component Mount */
  componentDidMount() {

  }

  /* Render */
  render() {
    console.log(this.props)
    return (
    <Main>

      {/* Tabs */}
      <MainTabs>
        <ButtonGrouped>Group Message</ButtonGrouped>
        <ButtonGrouped palette='secondary'>Take Action Now</ButtonGrouped>
        <ButtonGrouped palette='orange'>Town Hall</ButtonGrouped>
      </MainTabs>

      {/* Tray */}
      <MainTray>
          <Route exact path='/' component={OverviewTray}/>
      </MainTray>

      {/* Viewport */}
      <MainViewport>
        <Route exact path='/' component={GoogleMapFeed}/>
        <Route path='/test' component={Test}/>
        <Route path="/settings" component={Settings}/>
        { this.props.children }
      </MainViewport>
    </Main>
    )
  }
}
/* ---------------------------- Module Methods ------------------------------ */

/* Map State To Props */
const mapStateToProps = state => ({
  loading: true,
})

/* Map Dispatch To Props */
const mapDispatchToProps = (state) => ({
  mode: () => dispatch(),
  refresh: () => dispatch()
})
/* ---------------------------- Export Package ------------------------------ */
export default MainContainer

/* ----------------------------- Documentation ------------------------------ */
