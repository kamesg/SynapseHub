/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const HeaderState = styled.header`
  background-color: #dfdfdf;
  background-image:linear-gradient(
    0deg,
    rgba(214,214,214, 1) 0%,
    rgba(222,222,222, 1) 30%,
    rgba(232,232,232, 1) 50%,
    rgba(255,255,255, 1)
    )
  box-shadow: 0px 1px 0px rgba(0,0,0,0.19), 0px 1px 3px rgba(0, 0, 0, 0.16);
  bottom: 0; left: 0; top: 0;
  color: #3e3e3e;
  max-height: ${key(['layout', 'header','height'])};
  left: ${key(['layout', 'header','left'])};
  padding: 15px;
  position: fixed;
  width: ${key(['layout', 'header','width'])} ;
  z-index: ${key(['layout', 'header','zindex'])};
`

/* ------------------------- Component Properties --------------------------- */
HeaderState.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const HeaderStyled = (props) => {
  return (
    <HeaderState {...props}>
      {this.props.children}
    </HeaderState>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default HeaderState