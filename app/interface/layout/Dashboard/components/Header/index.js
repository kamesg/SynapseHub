/* ------------------------- External Dependencies -------------------------- */
import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

/* ------------------------- Internal Dependencies -------------------------- */
import Header from './styled-components/HeaderStyled'

/* ---------------------------- Module Package ------------------------------ */
class HeaderContainer extends Component {

  /* Static Prop Types */
  static propTypes = {
    mode: PropTypes.func,
    refresh: PropTypes.func
  }

  /* Component Mount */
  componentDidMount() {

  }

  /* Render */
  render() {
    return (
      <Header>
        <Link to="/"><h3>SynapseHub</h3></Link>
      </Header>
    )
  }
}

export default HeaderContainer
