/* ------------------------- External Dependencies -------------------------- */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import styled, { ThemeProvider} from 'styled-components'
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

/* ------------------------- Internal Dependencies -------------------------- */
import applicationTheme from 'theme'

/* ------------------------ Initialize Dependencies ------------------------- */
injectTapEventPlugin();

/* --------------------------- Styled Components ---------------------------- */
import DashboardWrapper from './styled-components/DashboardWrapper'
/* --------------------------- Child Components ----------------------------- */
import Sidebar from 'layout/Dashboard/components/Sidebar'
import Header from 'layout/Dashboard/components/Header'
import Main from 'layout/Dashboard/components/Main'

/* ---------------------------- Module Package ------------------------------ */
class Container extends Component {

  /*--- Property Types ---*/
  static propTypes = {

  }

  /*--- Default Properties ---*/
  static defaultProps = {

  }

  /*--- Component Mount ---*/
  componentDidMount() {
    
  }

  /*--- Render ---*/
  render() {
    return (
      <ThemeProvider theme={applicationTheme}>
        <MuiThemeProvider>
          <DashboardWrapper>
            <Header />
            <Sidebar />
            <Main />
          </DashboardWrapper>
      </MuiThemeProvider>
      </ThemeProvider>
    )
  }
}
/* ---------------------------- Module Methods ------------------------------ */

/* Map State To Props */
const mapStateToProps = state => ({
  loading: true,
})

/* Map Dispatch To Props */
const mapDispatchToProps = (state) => ({

})
/* ---------------------------- Export Package ------------------------------ */
export default connect(mapStateToProps, mapDispatchToProps)(Container)

/* ----------------------------- Documentation ------------------------------ */
