/* ------------------------- External Dependencies -------------------------- */
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const DashboardWrapper = styled.main`
  bottom: 0 ; left: 0; top: 0; bottom: 0;
  height: 100%;
  position: absolute;
  overflow: hidden;
  width: 100%;
`

/* ------------------------- Component Properties --------------------------- */
DashboardWrapper.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const DashboardStyled = ({ ...props }) => {
  return (
    <DashboardWrapper {...props}>
      {children}
    </DashboardWrapper>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default DashboardWrapper