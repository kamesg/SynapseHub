const themeGenerate = () => {

const wrapperPadding = (mode) => {
  switch(mode) {
    case 'full':
      return '0'
    break;
    default:
      return '20px'
  }
}

const sidebarWidthConst = "170px";
  /* ------------------------ Initialize Dependencies ------------------------- */
let header = {}
let sidebar = {}
const main = {}

/* ----------------------------- Theme Defaults ----------------------------- */
const layoutMode = "normal"
const layoutContext = "defaultss"
let primaryLayoutObject = "header"

let mainTray = true;
let mainTabs = true;

let displayWidthSeed = "90%"

let trayWidth = '';
let pulloutPadding = '';
let displayWidth = ''

let headerHeight = ''
let sidebarWidth = '170px'


const themeSettings = {
  mainWrapperMode: 'default'
}
/* ------------------------------ Theme State ------------------------------- */
const state = {
  header: {

  },
  sidebar: {

  },
  main: {
    wrapper: {
      mode: themeSettings.mainWrapperMode
    },
    viewport: {

    },
    tabs: {
      active: true
    },
    tray: {
      active: true
    }, 
  },
}

/* ------------------------------ Color Scheme ------------------------------- */
main.theme = {
  wrapper: {
    background: '#FFF'
  },
  viewport: {
    background: '#FFF',
    color: '#3e3e3e'
  },
  tabs: {
    background: '#FFF',
    color: ''
  },
  tray: {
    background: '#FFF',
    color: ''
  }
}


/* ------------------------------ Layout Mode ------------------------------- */
switch (layoutMode) {

  case 'normal':


    break;

  /*--- Minimal Mode ---*/
  case "minimal":

    break;

  /*--- Canvas Mode ---*/
  case "canvas": // Canvas Mode

      break;
  /*--- Expose Mode ---*/
  case "expose": // Expose Mode

      break;

  /*--- Default Mode ---*/
  /**
   * The default mode is a "standard" Dashboard view mode, balancing navigation, options and action.
   * It's designed to quickly allow navigation anywhere in the application via promoting components
   * to display information using the Default mode style guide.
   */
  default:
    main.layout = {
      wrapper: {
        padding: state.main.wrapper.mode === 'default' ? '20px' : '0'
      },
      viewport: {
        height: state.main.tabs.active ? '80%' : '100%',
        left: state.main.tray.active ? '275px' : '0',
        top: state.main.tabs.active ? '15%' : '0',
        width: state.main.tray.active ? 'calc(100% - 300px)' : '100%'
      },
      tabs: {
        height: state.main.tabs.active ? '10%' : '0',
        padding: state.main.tabs.active ? '10px 15px' : '0',
        width: '100%'
      },
      tray: {
        height: '100%',
        width: state.main.tray.active ? '250px' : '0'
      }
    }

}

if (state.main.tabs.active) {
  trayWidth = "100px"
  pulloutPadding = "20px"
  displayWidth = "calc(" + displayWidthSeed + " - " + trayWidth + ")";
} else {
  trayWidth = 0;
  pulloutPadding = 0;
  displayWidth = displayWidthSeed
}


main.layout = {
      wrapper: {
        padding: state.main.wrapper.mode === 'default' ? '20px' : '0'
      },
      viewport: {
        height: state.main.tabs.active ? '80%' : '100%',
        left: state.main.tray.active ? '275px' : '0',
        top: state.main.tabs.active ? '15%' : '0',
        width: state.main.tray.active ? 'calc(100% - 300px)' : '100%'
      },
      tabs: {
        height: state.main.tabs.active ? '10%' : '0',
        padding: state.main.tabs.active ? '10px 15px' : '0',
        width: '100%'
      },
      tray: {
        height: '100%',
        padding: '20px',
        width: state.main.tray.active ? '250px' : '0'
      }
    }

return {
  fonts: {
    body: "Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen,Ubuntu,Cantarell,'Open Sans','Helvetica Neue',sans-serif",
    heading: "Roboto,'Segoe UI',Roboto,Oxygen,Ubuntu,Cantarell,'Open Sans','Helvetica Neue',sans-serif",
    button: "Roboto,'Segoe UI',Roboto,Oxygen,Ubuntu,Cantarell,'Open Sans','Helvetica Neue',sans-serif"
  },
  palette: {
    white: ['#ffffff', '#ffffff'],
    grayscale: ['#FFFFFF', '#616161', '#9e9e9e', '#bdbdbd', '#e0e0e0', '#ffffff'],
    primary: ['#c61a1a', '#c61a1a', '#c61a1a', '#c61a1a'],
    secondary: ['#005caa', '#005caa', '#005caa', '#005caa'],
    danger: ['#D32F2F', '#F44336', '#F8877F', '#FFCDD2'],
    alert: ['#D32F2F', '#F44336', '#F8877F', '#FFCDD2'],
    success: ['#D32F2F', '#F44336', '#F8877F', '#FFCDD2'],
    red: ['#c61a1a', '#c61a1a', '#c61a1a', '#c61a1a'],
    blue: ['#005caa', '#005caa', '#005caa', '#005caa'],
    orange: ['#f8a800', '#f8a800', '#f8a800', '#f8a800'],
    purple: ['#a837b9', '#a837b9', '#a837b9', '#a837b9'],
  },
  layout: {
    offsetHeight: headerHeight ? headerHeight : '60px',
    offsetWidth: sidebarWidth ? sidebarWidth : '170px',
    display: "calc(100% - " + sidebarWidth + ")",
    main: {
      background: '#31434b',
      padding: '20px',
      display: {
        width: displayWidth
      },
      viewport: {
        background: main.theme.viewport.background,
        height: main.layout.viewport.height,
        left:  main.layout.viewport.left,
        top:  main.layout.viewport.top,
        width: main.layout.viewport.width,
      },
      tabs: {
        background: "#e0e4ed",
        height: main.layout.tabs.height,
        padding: main.layout.tabs.padding,
        width: main.layout.tabs.width,
      },
      tray: {
        background: "#77b0ca",
        mode: "default",
        height: main.layout.tray.height,
        padding: main.layout.tray.padding,
        top: main.layout.tabs.height,
        width: main.layout.tray.width,
      }
    },
    header: {
      loading: true,
      height: headerHeight ? headerHeight : '60px', 
      left: primaryLayoutObject == "sidebar" ? sidebarWidth : 0,
      width: primaryLayoutObject == "sidebar" ? "calc(100% - " + sidebarWidth + ")" : "100%",
      zindex: primaryLayoutObject == "sidebar" ? 20 : 15
    },
    sidebar: {
      loading: true,
      top: primaryLayoutObject == "sidebar" ? 0 : headerHeight ? headerHeight : '60px',
      width: sidebarWidth ? sidebarWidth : '170px',
      zindex: primaryLayoutObject == "sidebar" ? 15 : 10,
      theme: {
        background: ""
      , color: ""
      , link: {
        primary: {
            default: ""
          , hover: ""
          , active: ""
          , focus: ""
          , icon: {
              default: ""
            , switch: ""

          }
        },
        secondary: {
            default: ""
          , hover: ""
          , active: ""
          , focus: ""
        }
      }
      , color: ""
      }
    }
  }
}
}

const s = themeGenerate()

export default s
