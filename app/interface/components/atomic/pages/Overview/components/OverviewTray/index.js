/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { prop, ifProp } from 'styled-tools'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const Wrapper = styled.div`
  position: relative

  & *:first-of-type {
    margin-top: 0;
    padding-top: 0;
  }
`

/* ------------------------- Component Properties --------------------------- */
Wrapper.propTypes = {
  children: PropTypes.string,
}

/* ------------------------------- Component -------------------------------- */
const Component = ({ ...props }) => (
    <Wrapper {...props}>
      <h3>How It Works</h3>
      <p>Pellentesque eget odio sed ipsum rhoncus suscipit. Mauris dignissim lobortis pulvinar. Pellentesque hendrerit commodo fringilla. Proin suscipit mauris dui, sit amet dapibus tellus egestas non.</p>
      <p>Pellentesque eget odio sed ipsum rhoncus suscipit. Mauris dignissim lobortis pulvinar. Pellentesque hendrerit commodo fringilla. Proin suscipit mauris dui, sit amet dapibus tellus egestas non.</p>
      <p>Pellentesque eget odio sed ipsum rhoncus suscipit. Mauris dignissim lobortis pulvinar. Pellentesque hendrerit commodo fringilla. Proin suscipit mauris dui, sit amet dapibus tellus egestas non.</p>
    </Wrapper>
  )

/* ---------------------------- Export Package ------------------------------ */
export default Component