/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const AboutPage = styled.article`
  box-sizing: border-box;
`

/* ------------------------- Component Properties --------------------------- */
AboutPage.propTypes = {

}

/* ------------------------------- Component -------------------------------- */
const MainStyled = ({ ...props }) => {
  return (
    <AboutPage>
      <h1>About</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque dolor vitae diam efficitur pulvinar. Sed convallis, dui in consequat ornare, eros quam eleifend orci, a elementum sapien ligula sed turpis. Integer nec elit sit amet urna pretium ultrices ac eget mauris. Donec vel tortor arcu. Sed urna nibh, euismod ac quam scelerisque, malesuada aliquam odio. Sed ligula mauris, mattis in feugiat eu, ullamcorper quis augue. Aenean dictum imperdiet enim. Mauris elementum magna sapien, id volutpat nisl eleifend et. Curabitur magna libero, luctus sit amet nisi ac, faucibus feugiat turpis.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque dolor vitae diam efficitur pulvinar. Sed convallis, dui in consequat ornare, eros quam eleifend orci, a elementum sapien ligula sed turpis. Integer nec elit sit amet urna pretium ultrices ac eget mauris. Donec vel tortor arcu. Sed urna nibh, euismod ac quam scelerisque, malesuada aliquam odio. Sed ligula mauris, mattis in feugiat eu, ullamcorper quis augue. Aenean dictum imperdiet enim. Mauris elementum magna sapien, id volutpat nisl eleifend et. Curabitur magna libero, luctus sit amet nisi ac, faucibus feugiat turpis.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque dolor vitae diam efficitur pulvinar. Sed convallis, dui in consequat ornare, eros quam eleifend orci, a elementum sapien ligula sed turpis. Integer nec elit sit amet urna pretium ultrices ac eget mauris. Donec vel tortor arcu. Sed urna nibh, euismod ac quam scelerisque, malesuada aliquam odio. Sed ligula mauris, mattis in feugiat eu, ullamcorper quis augue. Aenean dictum imperdiet enim. Mauris elementum magna sapien, id volutpat nisl eleifend et. Curabitur magna libero, luctus sit amet nisi ac, faucibus feugiat turpis.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque dolor vitae diam efficitur pulvinar. Sed convallis, dui in consequat ornare, eros quam eleifend orci, a elementum sapien ligula sed turpis. Integer nec elit sit amet urna pretium ultrices ac eget mauris. Donec vel tortor arcu. Sed urna nibh, euismod ac quam scelerisque, malesuada aliquam odio. Sed ligula mauris, mattis in feugiat eu, ullamcorper quis augue. Aenean dictum imperdiet enim. Mauris elementum magna sapien, id volutpat nisl eleifend et. Curabitur magna libero, luctus sit amet nisi ac, faucibus feugiat turpis.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque dolor vitae diam efficitur pulvinar. Sed convallis, dui in consequat ornare, eros quam eleifend orci, a elementum sapien ligula sed turpis. Integer nec elit sit amet urna pretium ultrices ac eget mauris. Donec vel tortor arcu. Sed urna nibh, euismod ac quam scelerisque, malesuada aliquam odio. Sed ligula mauris, mattis in feugiat eu, ullamcorper quis augue. Aenean dictum imperdiet enim. Mauris elementum magna sapien, id volutpat nisl eleifend et. Curabitur magna libero, luctus sit amet nisi ac, faucibus feugiat turpis.</p>
    </AboutPage>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default MainStyled