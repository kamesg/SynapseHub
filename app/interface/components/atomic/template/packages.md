/* ------------------------- External Dependencies -------------------------- */

/* ------------------------- Internal Dependencies -------------------------- */

/* ------------------------ Initialize Dependencies ------------------------- */

/* ---------------------------- Module Package ------------------------------ */

/* ------------------------- Begin Module Methods --------------------------- */

/* ---------------------------- Export Package ------------------------------ */

/* ----------------------------- Documentation ------------------------------ */





/* ----------------------------------------------------------------------------- New Package Style ---------- */


/* ------------------------- External Dependencies -------------------------- */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import styledTheme, { font, palette } from 'styled-theme'
import styledTools, { ifProp } from 'styled-tools'

/* ------------------------- Internal Dependencies -------------------------- */

/* ------------------------ Initialize Dependencies ------------------------- */

/* ---------------------------- Module Package ------------------------------ */

/* ------------------------- Component Properties --------------------------- */

/* ------------------------------- Component -------------------------------- */

/* ---------------------------- Export Package ------------------------------ */

/* ----------------------------- Documentation ------------------------------ */


