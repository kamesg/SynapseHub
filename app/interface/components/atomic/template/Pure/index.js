/* ------------------------- External Dependencies -------------------------- */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { prop, ifProp } from 'styled-tools'
import { key, font, palette } from 'styled-theme'

/* ------------------------- Internal Dependencies -------------------------- */

/* --------------------------- Styled Components ---------------------------- */
const Wrapper = styled.div`
  position: relative

  & *:first-of-type {
    margin-top: 0;
    padding-top: 0;
  }
`

/* ------------------------- Component Properties --------------------------- */
Wrapper.propTypes = {
  children: PropTypes.string,
}

/* ------------------------------- Component -------------------------------- */
const Component = ({ ...props }) => {
  return (
    <Wrapper {...props}>
      {children}
    </Wrapper>
  )
}

/* ---------------------------- Export Package ------------------------------ */
export default Wrapper