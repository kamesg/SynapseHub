import React, { PropTypes } from 'react'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'

const Card = styled.div`
  background-color: #FFF;
  border-radius: 10px;
  color: ${palette('grayscale', 0)};
`

const Organism = (props) => {
  return (
    <Card {...props}>
      {this.props.children}
    </Card>
  )
}

Card.propTypes = {
  border: PropTypes.object,
}

export default Card
