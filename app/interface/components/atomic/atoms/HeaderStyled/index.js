import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'

const HeaderStyledWrapper = styled.div`
  display: inline-block;
`
const HeaderStyled = (props) => {
  return (
    <HeaderStyledWrapper {...props}>
      {this.props.children}
    </HeaderStyledWrapper>
  )
}

HeaderStyled.propTypes = {
  
}

export default HeaderStyled