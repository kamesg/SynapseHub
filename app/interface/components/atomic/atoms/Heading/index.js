import React, { PropTypes } from 'react'
import styled, { css } from 'styled-components'
import { font, palette } from 'styled-theme'

export const fontSizeRendered = ({ fontSize }) => `${0.75 + (1 * (1 / fontSize))}rem`
export const marginRendered = ({ margin }) => `${margin}`
export const display = ({display}) => `${display}`

const styles = css`
  display: ${display};
  font-family: ${font('primary')};
  font-weight: 500;
  font-size: ${fontSizeRendered};
  margin: ${marginRendered};
  color: ${palette({ grayscale: 0 }, 1)};
`

const Heading = styled(({ level, children, reverse, fontSize, palette, margin, display, theme, ...props }) =>
  React.createElement(`h${level}`, props, children)
)`${styles}`

Heading.propTypes = {
  level: PropTypes.number,
  children: PropTypes.node,
  palette: PropTypes.string,
  fontSize: PropTypes.string,
  margin: PropTypes.string,
  display: PropTypes.string,
  reverse: PropTypes.bool
}

Heading.defaultProps = {
  level: 1,
  fontSize: "1",
  display: "inline-block",
  margin: '10px 0 12.5px',
  palette: 'grayscale'
}

export default Heading
