/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';
import App from 'containers/App';

import Dashboard from 'layout/Dashboard'

import SelfCon from 'containers/routing/SelfContainedRouting'
import Routess from 'containers/routing/Routes'

const DashboardTab = () => (
  <div>
    <h1>Dashboardsss</h1>
    <SelfCon />
    <Routess />
  </div>
)

export default () => (
  <App>
    <Switch>
      <Route  path="/" component={Dashboard} />
    </Switch>
  </App>
);
