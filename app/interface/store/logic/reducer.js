// @flow
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import { routerReducer as routing } from 'react-router-redux';

const rootReducer = combineReducers({
  routing,
  form: formReducer
});

export default rootReducer;
