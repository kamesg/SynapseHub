import { initialState } from './selectors'
import {
    CREATE_CAMPAIGN
  , DELETE_CAMPAIGN
  , PAUSE_CAMPAIGN } from './actions'

export default (state = initialState, action) => {
  switch (action.type) {
    case CAMPAIGN_CREATE_SUCCESS:
      return {
        ...state,
        campaigns: [action.data, ...state.campaigns],
      }
    case DELETE_CAMPAIGN:
      return {
        ...state,
        list: action.list,
      }
    default:
      return state
  }
}
