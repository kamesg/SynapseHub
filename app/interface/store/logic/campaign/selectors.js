export const initialState = {
  campaignList: [],
}

export const getList = (state = initialState) => state.campaignList || initialState.campaignList
