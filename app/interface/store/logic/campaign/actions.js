// Create, Read, Update, Delete
export const CAMPAIGN_CREATE  = "CAMPAIGN_CREATE"
export const CAMPAIGN_CREATE_REQUEST  = "CAMPAIGN_CREATE_REQUEST"
export const CAMPAIGN_CREATE_SUCCESS  = "CAMPAIGN_CREATE_SUCCESS"
export const CAMPAIGN_CREATE_FAILURE  = "CAMPAIGN_CREATE_FAILURE"

export const CAMPAIGN_READ  = "CAMPAIGN_READ"
export const CAMPAIGN_READ_REQUEST  = "CAMPAIGN_READ_REQUEST"
export const CAMPAIGN_READ_SUCCESS  = "CAMPAIGN_READ_SUCCESS"
export const CAMPAIGN_READ_FAILURE  = "CAMPAIGN_READ_FAILURE"

export const CAMPAIGN_UPDATE  = "CAMPAIGN_UPDATE"
export const CAMPAIGN_UPDATE_REQUEST  = "CAMPAIGN_UPDATE_REQUEST"
export const CAMPAIGN_UPDATE_SUCCESS  = "CAMPAIGN_UPDATE_SUCCESS"
export const CAMPAIGN_UPDATE_FAILURE  = "CAMPAIGN_UPDATE_FAILURE"

export const CAMPAIGN_DELETE  = "CAMPAIGN_DELETE"
export const CAMPAIGN_DELETE_REQUEST  = "CAMPAIGN_DELETE_REQUEST"
export const CAMPAIGN_DELETE_SUCCESS  = "CAMPAIGN_DELETE_SUCCESS"
export const CAMPAIGN_DELETE_FAILURE  = "CAMPAIGN_DELETE_FAILURE"

export const CAMPAIGN_VISIBILITY_FILTER = "CAMPAIGN_VISIBILITY_FILTER"

// Action Creators
/* -------------------------------------------------------------------------- */
export const campaignCreate = {
    request: (data, resolve, reject) => ({ type: CAMPAIGN_CREATE_REQUEST, data, resolve, reject }),
    success: (data) => ({ type: CAMPAIGN_CREATE_SUCCESS, data}),
    failure: (error) => ({ type: CAMPAIGN_CREATE_FAILURE, error})
}

export const campaignRead = {
    request: (data, resolve, reject) => ({ type: CAMPAIGN_READ_REQUEST, data, resolve, reject }),
    success: (data) => ({ type: CAMPAIGN_READ_SUCCESS, data} ),
    failure: (error) => ({ type: CAMPAIGN_READ_FAILURE, error})
}

export const campaignUpdate = {
    request: (data, resolve, reject) => ({ type: CAMPAIGN_UPDATE_REQUEST, data, resolve, reject }),
    success: (data) => ({ type: CAMPAIGN_UPDATE_SUCCESS, data}),
    failure: (error) => ({ type: CAMPAIGN_UPDATE_FAILURE, error})
}

export const campaignDelete = {
    request: (data, resolve, reject) => ({ type: CAMPAIGN_DELETE_REQUEST, data, resolve, reject }),
    success: (data) => ({ type: CAMPAIGN_DELETE_SUCCESS}),
    failure: (error) => ({ type: CAMPAIGN_DELETE_FAILURE, error})
}
