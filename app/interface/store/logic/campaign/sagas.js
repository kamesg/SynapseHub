import { take, put, call, fork } from 'redux-saga/effects'
import {
          campaignCreate,
          campaignRead,
          campaignUpdate,
          campaignDelete,
          campaignList,
          CAMPAIGN_CREATE_REQUEST,
          CAMPAIGN_DELETE_REQUEST,
          CAMPAIGN_READ_REQUEST,
          CAMPAIGN_UPDATE_REQUEST
        } from './actions'

// Middleware
import SimpleStorage from 'storage/SimpleStorage'
const storage = new SimpleStorage;

export function* watchCampaignCreateRequest() {
  while (true) {
    const { data } = yield take(CAMPAIGN_CREATE_REQUEST)
    yield put(campaignCreate.success(data))
    yield call(createCampaign, data)
  }
}

export function* createCampaign(newData) {
  try {
    // const data = yield call(storage.set, newData)
    yield put(campaignCreate.success(data))
  } catch (e) {
    yield put (campaignCreate.failure(e))
  }
}

export function* watchCampaignReadRequest() {
  while (true) {
    const { data } = yield take(CAMPAIGN_CREATE_REQUEST)
    yield call(createCampaign, data)
  }
}

export function* watchCampaignUpdateRequest() {
  while (true) {
    const { data } = yield take(CAMPAIGN_CREATE_REQUEST)
    yield call(createCampaign, data)
  }
}

export function* watchCampaignDeleteRequest() {
  while (true) {
    const { data } = yield take(CAMPAIGN_CREATE_REQUEST)
    yield call(createCampaign, data)
  }
}
