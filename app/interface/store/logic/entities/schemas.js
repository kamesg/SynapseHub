import { schema } from 'normalizr'
import { POST_LIST_SUCCESS, POST_CREATE_SUCCESS } from '../post/actions'
import { ORGANIZATION_LIST_SUCCESS, ORGANIZATION_CREATE_SUCCESS } from '../organization/actions'
import { PLACES_LIST_SUCCESS, PLACES_CREATE_SUCCESS } from '../places/actions'
import { IDENTITY_LIST_SUCCESS, IDENTITY_CREATE_SUCCESS } from '../identity/actions'

export const post = new schema.Entity('post')
export const organization = new schema.Entity('organization')
export const places = new schema.Entity('places')
export const identity = new schema.Entity('identity')

export const actionsMeta = {
  [POST_LIST_SUCCESS]: { property: 'list', schema: [post] },
  [POST_CREATE_SUCCESS]: { property: 'data', schema: post },
  [ORGANIZATION_LIST_SUCCESS]: { property: 'list', schema: [organization] },
  [ORGANIZATION_CREATE_SUCCESS]: { property: 'data', schema: organization },
  [PLACES_LIST_SUCCESS]: { property: 'list', schema: [places] },
  [PLACES_CREATE_SUCCESS]: { property: 'data', schema: places },
  [IDENTITY_LIST_SUCCESS]: { property: 'list', schema: [identity] },
  [IDENTITY_CREATE_SUCCESS]: { property: 'data', schema: identity }
}
